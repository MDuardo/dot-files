# MDuardo's Dot Files

## Contents

This repository has configuration files for...

Terminal emulators:

- [Alacritty](https://github.com/alacritty/alacritty)
- [Ghostty](https://github.com/mitchellh/ghostty) (private repo for testers)
- [kitty](https://github.com/kovidgoyal/kitty)
- [WezTerm](https://github.com/wez/wezterm)

Text editors:

- [emacs](https://git.savannah.gnu.org/cgit/emacs.git)
- [Flow](https://github.com/neurocyte/flow)
- [helix](https://github.com/helix-editor/helix)
- [nvim](https://github.com/neovim/neovim)
- [VSCodium](https://github.com/VSCodium/vscodium)
- [Zed](https://github.com/zed-industries/zed)

CLI:

- [bash](https://savannah.gnu.org/projects/bash/)
- [bat](https://github.com/sharkdp/bat)
- [btop](https://github.com/aristocratos/btop)
- [fastfetch](https://github.com/fastfetch-cli/fastfetch)
- [fish](https://github.com/fish-shell/fish-shell)
- [lf](https://github.com/gokcehan/lf)
- [newsboat](https://github.com/newsboat/newsboat)
- [Starship](https://github.com/starship/starship)

GUI:

- [dunst](https://github.com/dunst-project/dunst)
- [gBar](https://github.com/scorpion-26/gBar)
- [hyprland](https://github.com/hyprwm/Hyprland)
- [hyprpaper](https://github.com/hyprwm/hyprpaper)
- [MangoHud](https://github.com/flightlessmango/MangoHud)
- [swaylock-effects](https://github.com/mortie/swaylock-effects)
- [wofi](https://hg.sr.ht/~scoopta/wofi)

## License

This project is released under the Unlicense, making it completely copyright
free and available to the public domain. Feel free to utilize it for any
purpose.
