eval "$(starship init bash)"

eval "$(zoxide init bash)"

exec fish

alias cd="z"
alias hx="helix"
alias ls="eza"
alias vim="nvim"

export EDITOR=nvim
export VISUAL=nvim

export PATH="$PATH:~/.nimble/bin"

export PATH="$PATH:~/.local/share/gem/ruby/3.0.0/bin"

export BUN_INSTALL="$HOME/.bun"
export PATH=$BUN_INSTALL/bin:$PATH

export PATH="$PATH:~/.config/emacs/bin"
export PATH="$PATH:~/.local/bin"
