(local options {
  :format_on_save {
    ;; These options will be passed to conform.format()
    :lsp_fallback true
    :timeout_ms 500}

  :formatters_by_ft {
    :c [:astyle]
    :cpp [:astyle]
    :crystal [:crystal]
    :cs [:astyle]
    :css [:prettier]
    :dart [:dart_format]
    ;:fennel [:fnlfmt] ;produces ugly code
    :fish [:fish_indent]
    :gleam [:gleam]
    :go [:gofmt]
    :html [:prettier]
    :java [:astyle]
    :javascript [:prettier]
    :json [:prettier]
    :jsonc [:prettier]
    :json5 [:prettier]
    ;:kotlin [:ktfmt] ;doesn't format, report this on conform.nvim
    :lua [:stylua]
    :markdown [:prettier]
    :nim [:nimpretty]
    :nix [:nixfmt]
    :perl [:perltidy]
    :php [:pretty-php]
    :python [:black]
    :ruby [:rufo]
    :rust [:rustfmt]
    :scss [:prettier]
    :sh [:beautysh]
    :typescript [:prettier]
    :yaml [:prettier]
    :zig [:zigfmt]
  }})

((. (require :conform) :setup) options)
