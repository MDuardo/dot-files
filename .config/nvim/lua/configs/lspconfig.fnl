(local on-attach (. (require :nvchad.configs.lspconfig) :on_attach))
(local on-init (. (require :nvchad.configs.lspconfig) :on_init))
(local capabilities (. (require :nvchad.configs.lspconfig) :capabilities))

(local lspconfig (require :lspconfig))

(local servers [
  :bashls
  :clangd
  :clojure_lsp
  :crystalline
  :dartls
  :fennel_ls
  :gleam
  :gopls
  :jdtls
  :julials
  :marksman
  :nim_langserver
  :ols
  :pylsp
  :racket_langserver
  :solargraph
  :rust_analyzer
  :tsserver
  :zls])

;; LSPs with default config
(each [_ lsp (ipairs servers)]
  ((. (. lspconfig lsp) :setup) {
    :on_attach on-attach
    :on_init on-init
    : capabilities}))
