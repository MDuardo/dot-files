;;;; This file  needs to have same structure as nvconfig.lua 
;;;; https://github.com/NvChad/NvChad/blob/v2.5/lua/nvconfig.lua

;;;; @type ChadrcConfig

(local M {})

(set M.ui {
  :theme :nord

  :theme_toggle {:nord :onenord_light}

  :hl_override {
    :Comment {:italic true}
    "@comment" {:italic true}}})

M
