[ {1
  :stevearc/conform.nvim
  :config (fn [] (require :configs.conform))
    :event :BufWritePre}

  ; You need this for the LSPs to work.
  {1
  :neovim/nvim-lspconfig
  :config (fn []
    ((. (require :nvchad.configs.lspconfig) :defaults))
    (require :configs.lspconfig))}

  ;; Portable package manager for Neovim.
  {1
  :williamboman/mason.nvim
  :opts {:ensure_installed [
    ;; LSPs
    ;:lua-language-server ;optional
    ;:html-lsp ;do not use unless you want to be sued
    ;:css-lsp ;do not use unless you want to be sued

    ;; Formatters
    ;:stylua
    ;:prettier
  ]}}

  ;; TreeSitter syntax highlighting.
  {1
  :nvim-treesitter/nvim-treesitter
  :opts {:ensure_installed [
    :bash
    :c
    :cpp
    :c_sharp
    :css
    :dart
    :djot
    :elixir
    :fennel
    :fish
    :gleam
    :go
    :hjson
    :html
    :java
    :javascript
    :json
    :jsonc
    :json5
    :julia
    :kotlin
    :lua
    :markdown
    :nim
    :nix
    :odin
    :org
    :perl
    :php
    :python
    :r
    :racket
    :ruby
    :rust
    :scheme
    :scss
    :toml
    :typescript
    :v
    :vim
    :yaml
    :zig]}}

  ;; Vim filetype and tools support for the Crystal language.
  {1
  :vim-crystal/vim-crystal
  :ft :crystal}]
