;; Neovim settings
(set vim.wo.relativenumber true)
(set vim.wo.wrap false)
(set vim.wo.list true)
(set vim.opt.colorcolumn :81)
(set vim.opt.listchars {:space "∙" :tab "󰌒 "})

;; Code Runner
(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term zig cc % -o %:r && ./%:r && rm %:r<CR>"
    {:noremap true}))
  :pattern [:c]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term zig c++ % -o %:r && ./%:r && rm %:r<CR>"
    {:noremap true}))
  :pattern [:cpp]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term crystal %<CR>"
    {:noremap true}))
  :pattern [:crystal]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term dotnet run<CR>"
    {:noremap true}))
  :pattern [:cs]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term dart %<CR>"
    {:noremap true}))
  :pattern [:dart]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term elixir %<CR>"
    {:noremap true}))
  :pattern [:elixir]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5>
      ":split | term fennel --compile % > %:r.lua && lua %:r.lua && rm %:r.lua<CR>"
    {:noremap true}))
  :pattern [:fennel]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term fish %<CR>"
    {:noremap true}))
  :pattern [:fish]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term gleam run<CR>"
    {:noremap true}))
  :pattern [:gleam]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term go run %<CR>"
    {:noremap true}))
  :pattern [:go]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term java %<CR>"
    {:noremap true}))
  :pattern [:java]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term bun %<CR>"
    {:noremap true}))
  :pattern [:javascript :typescript]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term julia %<CR>"
    {:noremap true}))
  :pattern [:julia]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5>
      ":split | term kotlinc % -include-runtime -d %:r.jar && java -jar %:r.jar && rm %:r.jar<CR>"
    {:noremap true}))
  :pattern [:kotlin]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term lua %<CR>"
    {:noremap true}))
  :pattern [:lua]})

(vim.api.nvim_create_autocmd [:BufRead :BufNewFile] {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term nim c -r --verbosity:0 % && rm %:r<CR>"
    {:noremap true}))
  :pattern [:*.nim]})

(vim.api.nvim_create_autocmd [:BufRead :BufNewFile] {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term nim --verbosity:0 %<CR>"
    {:noremap true}))
  :pattern [:*.nims]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term odin run % -file<CR>"
    {:noremap true}))
  :pattern [:odin]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term perl %<CR>"
    {:noremap true}))
  :pattern [:perl]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term php %<CR>"
    {:noremap true}))
  :pattern [:php]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term python %<CR>"
    {:noremap true}))
  :pattern [:python]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term Rscript %<CR>"
    {:noremap true}))
  :pattern [:r]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term racket %<CR>"
    {:noremap true}))
  :pattern [:racket :scheme]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term ruby %<CR>"
    {:noremap true}))
  :pattern [:ruby]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term cargo run<CR>"
    {:noremap true}))
  :pattern [:rust]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term bash %<CR>"
    {:noremap true}))
  :pattern [:sh]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term v run %<CR>"
    {:noremap true}))
  :pattern [:v]})

(vim.api.nvim_create_autocmd :FileType {
  :callback (fn []
    (vim.keymap.set :n :<F5> ":split | term zig run %<CR>"
    {:noremap true}))
  :pattern [:zig]})

;; Run on write
(vim.api.nvim_create_autocmd :BufWritePost {
  :callback (fn []
    (vim.cmd ":!fennel --compile % > %:r.lua")
    (print "Fennel -> Lua"))
  :pattern [:*.fnl]})

(vim.api.nvim_create_autocmd :BufWritePost {
  :callback (fn []
    (vim.cmd ":!sass % %:r.css")
    (print "Sass -> CSS"))
  :pattern [:*.sass :*.scss]})

(vim.api.nvim_create_autocmd :BufWritePost {
  :callback (fn []
    (vim.cmd "silent !ktfmt --kotlinlang-style %")
    (vim.cmd :edit))
  :pattern [:*.kt]})

;; Indentation
(vim.api.nvim_create_autocmd :FileType {
  :group (vim.api.nvim_create_augroup :Two_Spaces {:clear true})
  :command "setlocal autoindent expandtab shiftwidth=2 tabstop=2"
  :pattern [
    :crystal
    :css
    :dart
    :elixir
    :fennel
    :gleam
    :hjson
    :html
    :javascript
    :json
    :jsonc
    :json5
    :lua
    :nim
    :r
    :racket
    :ruby
    :scss
    :scheme
    :toml
    :typescript
    :yaml]})

(vim.api.nvim_create_autocmd :FileType {
  :group (vim.api.nvim_create_augroup :Four_Spaces {:clear true})
  :command "setlocal autoindent expandtab shiftwidth=4 tabstop=4"
  :pattern [
    :c
    :cpp
    :cs
    :fish
    :java
    :julia
    :kotlin
    :markdown
    :perl
    :php
    :python
    :rust
    :sh
    :zig]})

(vim.api.nvim_create_autocmd :FileType {
  :group (vim.api.nvim_create_augroup :Four_Tabs {:clear true})
  :command "setlocal autoindent noexpandtab shiftwidth=4 tabstop=4"
  :pattern [
    :go]})

(vim.api.nvim_create_autocmd :FileType {
  :group (vim.api.nvim_create_augroup :Eight_Tabs {:clear true})
  :command "setlocal autoindent noexpandtab shiftwidth=8 tabstop=8"
  :pattern [
    :odin
    :v]})

;; Line length
(vim.api.nvim_create_autocmd :FileType {
  :group (vim.api.nvim_create_augroup :100_Columns {:clear true})
  :command "setlocal colorcolumn=101"
  :pattern [
    :cs
    :fennel
    :racket
    :rust
    :scheme
    :zig]})

;; Verilog to Vlang
(vim.api.nvim_create_autocmd [:BufRead :BufNewFile] {
  :group (vim.api.nvim_create_augroup :FileType_V {:clear true})
  :command "setlocal filetype=v"
  :pattern [:*.v :*.vsh]})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; END OF NVIM CONFIG ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(set vim.g.base46_cache (.. (vim.fn.stdpath :data) :/nvchad/base46/))
(set vim.g.mapleader " ")

;; bootstrap lazy and all plugins
(local lazypath (.. (vim.fn.stdpath :data) :/lazy/lazy.nvim))

(when (not (vim.loop.fs_stat lazypath))
  (local repo "https://github.com/folke/lazy.nvim.git")
  (vim.fn.system
    [:git :clone "--filter=blob:none" repo :--branch=stable lazypath]))

(vim.opt.rtp:prepend lazypath)

(local lazy-config (require :configs.lazy))

;; load plugins
((. (require :lazy) :setup) [{1
  :NvChad/NvChad
  :branch :v2.5
  :config (fn [] (require :options))
  :import :nvchad.plugins
  :lazy false}

  {:import :plugins}] lazy-config)

;; load theme
(dofile (.. vim.g.base46_cache :defaults))
(dofile (.. vim.g.base46_cache :statusline))

(require :nvchad.autocmds)

(vim.schedule (fn [] (require :mappings)))
