set fish_greeting

starship init fish | source

zoxide init fish | source

alias cd="z"
alias hx="helix"
alias ls="eza"
alias vim="nvim"

set -x EDITOR nvim
set -x VISUAL nvim

set -x PATH ~/.nimble/bin $PATH

set -x PATH ~/.local/share/gem/ruby/3.0.0/bin $PATH

set -x BUN_INSTALL "$HOME/.bun"
set -x PATH $BUN_INSTALL/bin $PATH

set -x PATH ~/.config/emacs/bin $PATH
set -x PATH ~/.local/bin $PATH
