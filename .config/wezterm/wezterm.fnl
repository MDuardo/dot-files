(local wezterm (require :wezterm))
(local config {})

; Font configuration
(set config.font (wezterm.font_with_fallback [
  "JetBrainsMono NF" {
    :family "Symbols NF"
    :scale 0.85}]))

(set config.font_rules [{
  :intensity :Normal
  :italic true
  :font (wezterm.font {
    :family "JetBrainsMono NF"
    :weight :Regular
    :style :Italic})} {

  :intensity :Bold
  :italic false
  :font (wezterm.font {
    :family "JetBrainsMono NF"
    :weight :Bold
    :style :Normal})} {

  :intensity :Bold
  :italic true
  :font (wezterm.font {
    :family "JetBrainsMono NF"
    :weight :Bold
    :style :Italic})}])

(set config.font_size 13)

(set config.cell_width 1.2)

(set config.color_scheme :nord)

(set config.default_prog [:/usr/bin/fish :-l])

(set config.enable_wayland true)

(set config.hide_tab_bar_if_only_one_tab true)

(set config.window_decorations "TITLE | RESIZE")

(set config.window_padding {
  :bottom :5pt
  :left :5pt
  :right :5pt
  :top :5pt})

config
